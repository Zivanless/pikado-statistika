package program;

import java.util.Random;

/**
 * Created by Dystopia on 05-Jun-15.
 */
public final class RandomGaussian {

    private Random fRandom = new Random();

    public double getGaussian(double aMean, double aVariance){
        return aMean + fRandom.nextGaussian() * Math.sqrt(aVariance);
    }

    private static void log(Object aMsg){
        System.out.println(String.valueOf(aMsg));
    }
}
