package program;

/**
 * Created by Dystopia on 05-Jun-15.
 */
public class NormalnaRaspodela {
    private double varijansa;
    private double sredina;

    public NormalnaRaspodela(double sredina, double varijansa) {
        this.sredina = sredina;
        this.varijansa = varijansa;
    }

    public double getY(double x) {
        return Math.exp(-(x-sredina)*(x-sredina)/(2*varijansa))/Math.sqrt(2*Math.PI*varijansa);
    }

    public double getSredina() {
        return sredina;
    }

    public double getVar() {
        return varijansa;
    }

    public double getSigma() {
        return Math.sqrt(varijansa);
    }

}
