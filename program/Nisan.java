package program;

import java.awt.geom.Point2D;

/**
 * Created by Dystopia on 05-Jun-15.
 */
public class Nisan {
    private ImageDrawer podaci;

    public Nisan(ImageDrawer podaci) {
        this.podaci = podaci;
    }

    Point2D nisani() {
        double Max = podaci.matricaOcekivanja[0][0];
        int xMax = 0;
        int yMax = 0;
        for (int i = 0; i < podaci.matricaOcekivanja.length; i++)
            for (int j = 0; j < podaci.matricaOcekivanja[0].length; j++) {
                if (Max < podaci.matricaOcekivanja[i][j]){
                    Max = podaci.matricaOcekivanja[i][j];
                    xMax = i;
                    yMax = j;
                }
            }

        return new Point2D.Double((double)xMax/podaci.MapaVrednosti.getWidth(),(double)yMax/podaci.MapaVrednosti.getHeight());
    }

    //public void setPodaci(Image)
}
