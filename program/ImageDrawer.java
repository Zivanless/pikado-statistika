package program;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Dystopia on 05-Jun-15.
 */
public class ImageDrawer extends Canvas{
    BufferedImage MapaVrednosti;
    private BufferedImage MapaOcekivanja;
    private NormalnaRaspodela Gaus;
    private double sigma;
    private boolean heatmapShow = false;
    private Image crtanje1;
    private Image vrednosti;
    double[][] matricaOcekivanja;
    private Platno crtac;
    public ImageDrawer(BufferedImage MapaVrednosti, double sigma, Platno crtac) {
        super();
        this.crtac = crtac;
        this.sigma = sigma;
        Gaus = new NormalnaRaspodela(0, sigma*sigma);
        this.MapaVrednosti = MapaVrednosti;
        matricaOcekivanja = new double[MapaVrednosti.getWidth()][MapaVrednosti.getHeight()];
        vrednosti = MapaVrednosti.getScaledInstance(512,512,Image.SCALE_SMOOTH);
        racunajOcekivanje();
    }

    public void promeniSigma(double sigma) {
        this.sigma = sigma;
        crtac.setSigma(sigma);
        Gaus = new NormalnaRaspodela(0, sigma*sigma);
        racunajOcekivanje();
    }

    private BufferedImage IzracunajOcekivanja() {
        BufferedImage ocekivanja = new BufferedImage(MapaVrednosti.getWidth(), MapaVrednosti.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        double expmax = 0;
        double exp = 0;
        for (int i = 0; i < MapaVrednosti.getWidth(); i++)
            for (int j = 0; j < MapaVrednosti.getHeight(); j++)
            {
                int lowerX = Math.max(0,(int)(i-2*sigma));
                int upperX = Math.min(MapaVrednosti.getWidth()-1, (int) (i + 2*sigma));
                int lowerY = Math.max(0, (int) (j - 2*sigma));
                int upperY = Math.min(MapaVrednosti.getHeight()-1, (int) (j + 2*sigma));
                exp = 0;
                for (int k = lowerX; k <= upperX; k++)
                    for (int l = lowerY; l <= upperY; l++)
                    {
                        exp += Gaus.getY(Math.hypot(i-k,j-l))*(MapaVrednosti.getRGB(k,l)&0xFF);
                    }

                if (exp > expmax)
                    expmax = exp;
                matricaOcekivanja[i][j] = exp;
            }

            if (expmax != 0) {
                double koef = 255/expmax;
                for (int i = 0; i < MapaVrednosti.getWidth(); i++)
                    for (int j = 0; j < MapaVrednosti.getHeight(); j++)
                    {
                        int vrednost = (int) (matricaOcekivanja[i][j]*koef);
                        ocekivanja.setRGB(i,j,vrednost | vrednost << 8 | vrednost << 16);
                    }
            }

        return ocekivanja;
    }

    private void racunajOcekivanje() {
        MapaOcekivanja = IzracunajOcekivanja();
        crtanje1 = MapaOcekivanja.getScaledInstance(512,512,Image.SCALE_SMOOTH);
    }

    public void paint(Graphics g) {
       if (heatmapShow && crtanje1 != null)
            g.drawImage(crtanje1, 0, 0, this);
        else
         g.drawImage(vrednosti,0,0,this);
    }

    public void setHeat(boolean x) {
        heatmapShow = x;
        repaint();
    }

    public double getSigma() {
        return sigma;
    }
}
