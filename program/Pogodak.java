package program;

import java.awt.Color;
import java.awt.Graphics;

public class Pogodak extends Thread {

	private int x, y;
	private ImageDrawer platno;
	private boolean started = false;
	private Color color;
	
	public Pogodak(ImageDrawer platno, double x, double y, Color color){
		this.platno = platno;
		this.x = (int)(platno.getWidth()*x);
		this.y = (int)(platno.getHeight()*y);
		this.color = color;
	}
	
	public void crtaj(){
		start();
	}
	
	@Override
	public void run(){
		try{
			Graphics g = platno.getGraphics();
			for(int i = 0; i<3 && !interrupted(); i++){
				g.setColor(color);
				g.fillOval(x-5, y-5, 10, 10);
				Thread.sleep(400);
				platno.repaint();
				Thread.sleep(400);
			}
		}catch(InterruptedException e){}
	}
	
}
