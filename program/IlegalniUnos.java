package program;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Event;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.Panel;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class IlegalniUnos extends Dialog {

	private String poruka;
	
	public IlegalniUnos(Frame parent, String poruka){
        super(parent, true);
        this.poruka = poruka;
        setTitle("Ilegalni Unos! ");
        setBackground(Color.WHITE);
        setLayout(new BorderLayout());
        Panel panel = new Panel();
        panel.add(new Button("Zatvori"));
        add("South", panel);
        setSize(300,200);

        addWindowListener(new WindowAdapter() {
           public void windowClosing(WindowEvent windowEvent){
              dispose();
           }
        });
        
        setVisible(true);
     }

     public boolean action(Event evt, Object arg){
        if(arg.equals("Zatvori")){
           dispose();
           return true;
        }
        return false;
     }

     public void paint(Graphics g){
        g.setColor(Color.BLACK);
        g.drawString(poruka, 25, 70);
     }

}
