package program;

import java.awt.*;

public class Platno extends Canvas {

	private double scaleX, scaleY;
    private static int brojTacaka = 200;
    private static double borderX = 2, borderY=0.75;
    private static double korak = 2*borderX/(brojTacaka-1);
	private NormalnaRaspodela Gaus;

	public Platno(NormalnaRaspodela Gaus) {
        this.Gaus = Gaus;
        scaleX = scaleY = 1;
	}



    public void paint(Graphics g) {
        int width = getWidth();
        int height = getHeight();
        g.setColor(Color.BLACK);
        g.drawString("-"+borderX,5,height/2+15);
        g.drawString(Double.toString(borderX),width-20,height/2+15);
        g.drawLine(0,height/2,width,height/2);
        g.drawLine(width/2,0,width/2,height);
        g.drawString("-"+borderY,width/2+5,height-5);
        g.drawString(Double.toString(borderY),width/2+5,10);
        for (double i = -borderX; i <= borderX-korak; i += korak) {
            double x1 = (i+borderX)/(2*borderX)*width;
            double y1 = height-(Gaus.getY(i)+borderY)/(2*borderY)*height;
            double x2 = (i+korak+borderX)/(2*borderX)*width;
            double y2 = height-(Gaus.getY(i+korak)+borderY)/(2*borderY)*height;
            g.drawLine((int)x1, (int) y1,(int) x2,(int) y2);
        }
    }

    public void setSigma(double sigma) {
        Gaus = new NormalnaRaspodela(0, sigma*sigma);
        repaint();
    }


}
