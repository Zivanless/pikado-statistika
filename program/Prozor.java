package program;

import javax.imageio.ImageIO;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;

public class Prozor extends Frame {

	private Panel panelLvl1Center;
	private Panel panelLvl1Down;
	private Panel panelLvl2Center;
	private Panel panelLvl2Right;
	
	private ImageDrawer imageMap;
	private BufferedImage slika;
	private Platno platnoUp, platnoDown;
	
	private TextField textNumberOfThrows;
	private Button buttonNumberOfThrows;
	
	private TextField textSigma;
	private Button buttonSigma;
	
	private TextField textFileName;
	private Button buttonFileName;
	
	private Checkbox heatBox;
	
	private Gadjac gadjac;
	private Nisan nisan;
	
	public Prozor(){
		super("Pikado");
		panelLvl1Center = new Panel(new BorderLayout());
		panelLvl1Down = new Panel(new FlowLayout());
		
		panelLvl2Center = new Panel(new BorderLayout());
		panelLvl2Right = new Panel(new BorderLayout());


        File img = new File("slika.png");
        BufferedImage in = null;
        try {
            in = ImageIO.read(img);
        }catch (Exception e) {
        	new IlegalniUnos(Prozor.this, "File sa imenom 'slika.png' ne postoji! ");
        	dispose();
        	return;
        }

        slika = new BufferedImage(
                in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_ARGB);

        if(slika.getHeight()>64 || slika.getWidth()>64){
        	new IlegalniUnos(Prozor.this, "Dimenzije ne smeju da budu vece od 64 piksela! ");
        	dispose();
        	return;
        }
        Graphics2D g = slika.createGraphics();
        g.drawImage(in, 0, 0, null);
        g.dispose();
		platnoUp = new Platno(new NormalnaRaspodela(0,1));

		platnoDown = new Platno(new NormalnaRaspodela(0,4));

		imageMap = new ImageDrawer(slika, 1, platnoDown);
		
		textNumberOfThrows = new TextField(10);
		buttonNumberOfThrows = new Button("Gadjaj");
		
		heatBox = new Checkbox("Heat mapa");
		
		textSigma = new TextField(10);
		buttonSigma = new Button("Pokreni");
		
		textFileName = new TextField(30);
		buttonFileName = new Button("Otvori");
		
		setListeners();
		
		imageMap.setSize(512, 512);
		panelLvl2Center.add(imageMap, BorderLayout.CENTER);
		panelLvl2Center.setBackground(Color.LIGHT_GRAY);
		
		platnoUp.setSize(250, 250);
		platnoDown.setSize(250, 250);
		Panel panelLvl3Up = new Panel(new BorderLayout());
		panelLvl3Up.add(new Label("Objektivno odstupanje"), BorderLayout.NORTH);
		panelLvl3Up.add(platnoUp, BorderLayout.CENTER);
		
		Panel panelLvl3Down = new Panel(new BorderLayout());
		panelLvl3Down.add(new Label("Subjektivno odstupanje"), BorderLayout.NORTH);
		panelLvl3Down.add(platnoDown, BorderLayout.CENTER);
		//panelLvl2Right.setBackground(Color.LIGHT_GRAY);
		
		panelLvl2Right.add(panelLvl3Up, BorderLayout.NORTH);
		panelLvl2Right.add(panelLvl3Down, BorderLayout.CENTER);
		
		panelLvl1Center.add(panelLvl2Center, BorderLayout.CENTER);
		panelLvl1Center.add(panelLvl2Right, BorderLayout.EAST);
		
		Panel panelLvl2Left = new Panel(), panelLvl2Mid = new Panel(), panelLvl2Right = new Panel();
		
		panelLvl2Left.setBackground(Color.LIGHT_GRAY);
		panelLvl2Mid.setBackground(Color.LIGHT_GRAY);
		panelLvl2Right.setBackground(Color.LIGHT_GRAY);
		
		panelLvl2Left.add(textNumberOfThrows);
		panelLvl2Left.add(buttonNumberOfThrows);
		panelLvl2Left.add(heatBox);
		
		panelLvl2Mid.add(textSigma);
		panelLvl2Mid.add(buttonSigma);
		
		//panelLvl2Right.add(textFileName);
		//panelLvl2Right.add(buttonFileName);
		
		panelLvl1Down.add(panelLvl2Left);
		panelLvl1Down.add(panelLvl2Mid);
		//panelLvl1Down.add(panelLvl2Right);
		
		setLayout(new BorderLayout());
		add(panelLvl1Center, BorderLayout.CENTER);
		add(panelLvl1Down, BorderLayout.SOUTH);
		
		setSize(750, 575);
		setVisible(true);
		
		gadjac = new Gadjac(1, imageMap);
		nisan = new Nisan(imageMap);
		gadjac.istreniraj();
		
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e){
				
				dispose();
			}
		});
	}
	
	private void setListeners(){
		buttonNumberOfThrows.addActionListener(new ActionListener(){

			Pogodak pog;
			Pogodak gadjano;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Point2D gadjan = null;
				Point2D pogodjeno = null;
				try{
					int i = Integer.parseInt(textNumberOfThrows.getText());
					for(; i>0; i--){
						gadjan = nisan.nisani();// ono sto zeli da pogodi
						pogodjeno = gadjac.gadjaj(new Point2D.Double(slika.getWidth()*gadjan.getX(), slika.getHeight()*gadjan.getY()));
					}
					if(gadjano != null)
						gadjano.interrupt();
					gadjano = new Pogodak(imageMap, gadjan.getX(), gadjan.getY(), Color.BLUE);
					gadjano.crtaj();
					if(pog != null)
						pog.interrupt();
					pog = new Pogodak(imageMap, pogodjeno.getX()/slika.getWidth(), pogodjeno.getY()/slika.getHeight(), Color.RED);
					pog.crtaj();
				}catch(Exception exp){
					if(textNumberOfThrows.getText().equals("")){
						gadjan = nisan.nisani();// ono sto zeli da pogodi
						pogodjeno = gadjac.gadjaj(new Point2D.Double(slika.getWidth()*gadjan.getX(), slika.getHeight()*gadjan.getY()));
						if(gadjano != null)
							gadjano.interrupt();
						gadjano = new Pogodak(imageMap, gadjan.getX(), gadjan.getY(), Color.BLUE);
						gadjano.crtaj();
						if(pog != null)
							pog.interrupt();
						pog = new Pogodak(imageMap, pogodjeno.getX()/slika.getWidth(), pogodjeno.getY()/slika.getHeight(), Color.RED);
						pog.crtaj();
						return;
					}
					new IlegalniUnos(Prozor.this, "Unesena vrednost nije prirodan broj! ");
				}
			}
			
		});
		
		buttonSigma.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try{
					double sigma = Double.parseDouble(textSigma.getText());
					if(sigma>4){
						new IlegalniUnos(Prozor.this, "Nije dozvoljeno sigma > 4 ! ");
						return;
					}
					if(sigma<0){
						new IlegalniUnos(Prozor.this, "Nije dozvoljeno sigma < 0 ! ");
						return;
					}
					platnoDown = new Platno(new NormalnaRaspodela(0,4));
					//platnoUp = new Platno(new NormalnaRaspodela(0,sigma));
					platnoUp.setSigma(sigma);
					gadjac = new Gadjac(sigma, imageMap);
					nisan = new Nisan(imageMap);
					gadjac.istreniraj();
				}catch(Exception exp){
					new IlegalniUnos(Prozor.this, "Unesena vrednost nije dozvoljena! ");
				}
			}
			
		});
		
		buttonFileName.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		heatBox.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                imageMap.setHeat(heatBox.getState());

            }

        });
	}
	
}
