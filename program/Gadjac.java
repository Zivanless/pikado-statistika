package program;

import java.awt.geom.Point2D;

/**
 * Created by Dystopia on 05-Jun-15.
 */
public class Gadjac {
    private double sigma;
    private boolean istreniran = false;
    private int brojPogodaka;
    private ImageDrawer podaci;
    private RandomGaussian normalnaGenerator = new RandomGaussian();

    public Gadjac(double sigma, ImageDrawer podaci) {
        this.sigma = sigma;
        this.podaci = podaci;
        brojPogodaka = 0;
    }

    public void istreniraj() {
        if (!istreniran) {
            Point2D target;
            double subjektivnoSigma = 0;
            for (int i = 0; i < 2; i++) {
                target = gadjaj(new Point2D.Double(0, 0));
                subjektivnoSigma += Math.pow(Math.hypot(target.getX(), target.getY()), 2);
            }
            brojPogodaka += 2;
            podaci.promeniSigma(Math.sqrt(subjektivnoSigma));
            istreniran = true;
        }
    }

    public Point2D gadjaj(Point2D meta) {
        double radius = normalnaGenerator.getGaussian(0, sigma*sigma);
        if (istreniran) {
            double novoSigma = Math.sqrt((Math.pow(podaci.getSigma(), 2) * (brojPogodaka - 1) + radius * radius) / brojPogodaka);
            podaci.promeniSigma(novoSigma);
            brojPogodaka++;
        }
        double ugao = Math.random()*Math.PI;
        Point2D p2d = new Point2D.Double(meta.getX()+Math.cos(ugao)*radius, meta.getY()+Math.sin(ugao)*radius);
        return p2d;
    }

    public void promeniDrawer(ImageDrawer x) {
        podaci = x;
    }
}
